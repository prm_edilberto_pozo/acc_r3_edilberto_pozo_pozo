--------------------------------------------------------
-- Archivo creado  - jueves-diciembre-15-2016   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Table PARTIDOS
--------------------------------------------------------

  CREATE TABLE "PROYECTO"."PARTIDOS" 
   (	"ID_LOCAL" NUMBER(6,0), 
	"ID_VISITANTE" NUMBER(6,0), 
	"ID_CAMPO" NUMBER(6,0), 
	"FECHA" DATE, 
	"ID" NUMBER(6,0)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM" ;
--------------------------------------------------------
--  DDL for Table CAMPOS
--------------------------------------------------------

  CREATE TABLE "PROYECTO"."CAMPOS" 
   (	"ID_CAMPO" NUMBER(6,0), 
	"NOMBRE" VARCHAR2(20 BYTE), 
	"DESCRIPCION" VARCHAR2(100 BYTE), 
	"DIMENSIONES" VARCHAR2(20 BYTE), 
	"DIRECCION" VARCHAR2(70 BYTE), 
	"CAPACIDAD" NUMBER(10,0)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM" ;
--------------------------------------------------------
--  DDL for Table EQUIPOS
--------------------------------------------------------

  CREATE TABLE "PROYECTO"."EQUIPOS" 
   (	"ID" NUMBER(6,0), 
	"NOMBRE" VARCHAR2(20 BYTE), 
	"ID_CAMPO" NUMBER(6,0), 
	"NACIMIENTO" NUMBER(4,0), 
	"PAIS" VARCHAR2(20 BYTE), 
	"CAPITAN" VARCHAR2(20 BYTE), 
	"ENTRENADOR" VARCHAR2(20 BYTE)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM" ;
REM INSERTING into PROYECTO.PARTIDOS
SET DEFINE OFF;
Insert into PROYECTO.PARTIDOS (ID_LOCAL,ID_VISITANTE,ID_CAMPO,FECHA,ID) values ('4444','5555','333',to_date('13/12/16','DD/MM/RR'),'3');
Insert into PROYECTO.PARTIDOS (ID_LOCAL,ID_VISITANTE,ID_CAMPO,FECHA,ID) values ('2222','4444','222',to_date('13/12/16','DD/MM/RR'),'4');
Insert into PROYECTO.PARTIDOS (ID_LOCAL,ID_VISITANTE,ID_CAMPO,FECHA,ID) values ('2222','3333','222',to_date('13/12/16','DD/MM/RR'),'1');
Insert into PROYECTO.PARTIDOS (ID_LOCAL,ID_VISITANTE,ID_CAMPO,FECHA,ID) values ('1111','5555','111',to_date('13/12/16','DD/MM/RR'),'2');
Insert into PROYECTO.PARTIDOS (ID_LOCAL,ID_VISITANTE,ID_CAMPO,FECHA,ID) values ('2222','3333','222',to_date('13/12/16','DD/MM/RR'),'5');
Insert into PROYECTO.PARTIDOS (ID_LOCAL,ID_VISITANTE,ID_CAMPO,FECHA,ID) values ('2222','3333','222',to_date('13/12/16','DD/MM/RR'),'6');
Insert into PROYECTO.PARTIDOS (ID_LOCAL,ID_VISITANTE,ID_CAMPO,FECHA,ID) values ('2222','2222','222',to_date('13/12/16','DD/MM/RR'),'7');
Insert into PROYECTO.PARTIDOS (ID_LOCAL,ID_VISITANTE,ID_CAMPO,FECHA,ID) values ('4444','5555','222',to_date('31/12/16','DD/MM/RR'),'8');
REM INSERTING into PROYECTO.CAMPOS
SET DEFINE OFF;
Insert into PROYECTO.CAMPOS (ID_CAMPO,NOMBRE,DESCRIPCION,DIMENSIONES,DIRECCION,CAPACIDAD) values ('111','Nanana Arena','Desc  de nanana Arena','180x256','C: Nanana N: 99','10000');
Insert into PROYECTO.CAMPOS (ID_CAMPO,NOMBRE,DESCRIPCION,DIMENSIONES,DIRECCION,CAPACIDAD) values ('222','Quijote Arena','Pabell�n en ciu','1000x1800','Ciudad Real','20000');
Insert into PROYECTO.CAMPOS (ID_CAMPO,NOMBRE,DESCRIPCION,DIMENSIONES,DIRECCION,CAPACIDAD) values ('333','NANANA STADIUM','Estadio gigante','10k x 10k','NANANALANDIA','1000000');
REM INSERTING into PROYECTO.EQUIPOS
SET DEFINE OFF;
Insert into PROYECTO.EQUIPOS (ID,NOMBRE,ID_CAMPO,NACIMIENTO,PAIS,CAPITAN,ENTRENADOR) values ('1111','Team Nanana','111','2015','Spain','EDI','EDI');
Insert into PROYECTO.EQUIPOS (ID,NOMBRE,ID_CAMPO,NACIMIENTO,PAIS,CAPITAN,ENTRENADOR) values ('2222','BM Alarcos','222','2012','Espa�a','Jota','Mister');
Insert into PROYECTO.EQUIPOS (ID,NOMBRE,ID_CAMPO,NACIMIENTO,PAIS,CAPITAN,ENTRENADOR) values ('3333','BM Ciudad Real','222','1991','Spain','Juan','Luis');
Insert into PROYECTO.EQUIPOS (ID,NOMBRE,ID_CAMPO,NACIMIENTO,PAIS,CAPITAN,ENTRENADOR) values ('4444','Terrinches CF',null,'1888','Espa�a','Manolo','Tito');
Insert into PROYECTO.EQUIPOS (ID,NOMBRE,ID_CAMPO,NACIMIENTO,PAIS,CAPITAN,ENTRENADOR) values ('5555','NANANA e-Sports','333','2015','Canada','Mixwell','rush');
--------------------------------------------------------
--  DDL for Index PARTIDOS_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "PROYECTO"."PARTIDOS_PK" ON "PROYECTO"."PARTIDOS" ("ID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM" ;
--------------------------------------------------------
--  DDL for Index CAMPOS_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "PROYECTO"."CAMPOS_PK" ON "PROYECTO"."CAMPOS" ("ID_CAMPO") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM" ;
--------------------------------------------------------
--  DDL for Index EQUIPOS_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "PROYECTO"."EQUIPOS_PK" ON "PROYECTO"."EQUIPOS" ("ID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM" ;
--------------------------------------------------------
--  Constraints for Table PARTIDOS
--------------------------------------------------------

  ALTER TABLE "PROYECTO"."PARTIDOS" MODIFY ("ID" NOT NULL ENABLE);
  ALTER TABLE "PROYECTO"."PARTIDOS" ADD CONSTRAINT "PARTIDOS_PK" PRIMARY KEY ("ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM"  ENABLE;
--------------------------------------------------------
--  Constraints for Table CAMPOS
--------------------------------------------------------

  ALTER TABLE "PROYECTO"."CAMPOS" ADD CONSTRAINT "CAMPOS_PK" PRIMARY KEY ("ID_CAMPO")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM"  ENABLE;
  ALTER TABLE "PROYECTO"."CAMPOS" MODIFY ("NOMBRE" NOT NULL ENABLE);
  ALTER TABLE "PROYECTO"."CAMPOS" MODIFY ("ID_CAMPO" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table EQUIPOS
--------------------------------------------------------

  ALTER TABLE "PROYECTO"."EQUIPOS" ADD CONSTRAINT "EQUIPOS_PK" PRIMARY KEY ("ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM"  ENABLE;
  ALTER TABLE "PROYECTO"."EQUIPOS" MODIFY ("NOMBRE" NOT NULL ENABLE);
  ALTER TABLE "PROYECTO"."EQUIPOS" MODIFY ("ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Ref Constraints for Table PARTIDOS
--------------------------------------------------------

  ALTER TABLE "PROYECTO"."PARTIDOS" ADD CONSTRAINT "PARTIDOS_FK_CAMPO" FOREIGN KEY ("ID_CAMPO")
	  REFERENCES "PROYECTO"."CAMPOS" ("ID_CAMPO") ENABLE;
  ALTER TABLE "PROYECTO"."PARTIDOS" ADD CONSTRAINT "PARTIDOS_FK_LOCAL" FOREIGN KEY ("ID_LOCAL")
	  REFERENCES "PROYECTO"."EQUIPOS" ("ID") ENABLE;
  ALTER TABLE "PROYECTO"."PARTIDOS" ADD CONSTRAINT "PARTIDOS_FK_VISITANTE" FOREIGN KEY ("ID_VISITANTE")
	  REFERENCES "PROYECTO"."EQUIPOS" ("ID") ENABLE;
