/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package r3datos;

import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.exception.ConstraintViolationException;

/**
 *
 * @author Edilberto Pozo Pozo
 */
public class Modificaciones {
    public Modificaciones(){
        
    }
    /**
     * Actualiza/modifica un partido
     * @param id del partido
     * @param loc id del local
     * @param vis id del visitante
     * @param camp id del campo
     * @param fecha fecha del partido
     */
    public static void update(int id, int loc, int vis, int camp, Date fecha){
            
            Session session = Conexion.getSession(); //Abrimos una sesión llamando a la clase conexion
            Transaction tx = session.beginTransaction(); //Creamos una transacción

            Campos cp = new Campos();
            cp= (Campos) session.get(Campos.class,camp);
            Equipos eq = new Equipos();
            eq= (Equipos) session.get(Equipos.class,loc);
            Equipos eq1 = new Equipos();
            eq1= (Equipos) session.get(Equipos.class,vis);
            
            Partidos par = (Partidos)session.get(Partidos.class, id); 
            par.setCampos(cp);
            par.setEquiposByIdLocal(eq);
            par.setEquiposByIdVisitante(eq1);
            par.setFecha(fecha);
            session.update(par);
            
            try {

               tx.commit(); //Confirmamos la transacción

            } catch (ConstraintViolationException e) {

                System.out.println("ERROR");
                System.out.printf("MENSAJE: %s%n",e.getMessage());
                System.out.printf("COD ERROR: %d%n",e.getErrorCode());
                System.out.printf("ERROR SQL: %s%n" , e.getSQLException().getMessage());

            }
            
            session.close();
    }
    /**
     * Inserta un partido
     * @param id del partido
     * @param loc id del local
     * @param vis id del visitante
     * @param camp id del campo
     * @param fecha fecha del partido
     */
    public static void insert(int id, int loc, int vis, int camp, Date fecha){
            
            Session session = Conexion.getSession(); //Abrimos una sesión llamando a la clase conexion
            Transaction tx = session.beginTransaction(); //Creamos una transacción

            Campos cp = new Campos();
            cp= (Campos) session.get(Campos.class,camp);
            Equipos eq = new Equipos();
            eq= (Equipos) session.get(Equipos.class,loc);
            Equipos eq1 = new Equipos();
            eq1= (Equipos) session.get(Equipos.class,vis);
            
            Partidos par = new Partidos();
            par.setCampos(cp);
            par.setEquiposByIdLocal(eq);
            par.setEquiposByIdVisitante(eq1);
            par.setId(id);
            par.setFecha(fecha);
            session.save(par);
            
            try {

               tx.commit(); //Confirmamos la transacción

            } catch (ConstraintViolationException e) {

                System.out.println("ERROR");
                System.out.printf("MENSAJE: %s%n",e.getMessage());
                System.out.printf("COD ERROR: %d%n",e.getErrorCode());
                System.out.printf("ERROR SQL: %s%n" , e.getSQLException().getMessage());

            }
            session.close();
    }
    /**
     * Ver lista de partidos
     * @return Iterator con la lista de partidos
     */
    public static Iterator getLista(){
        Session session = Conexion.getSession(); //Abrimos una sesión llamando a la clase conexion
        //Añadimos los Partidos al Text Area y los ids al combobox
        Query q = session.createQuery("from Partidos");
        Iterator iter = q.iterate();
        
        return iter;
    }
    /**
     * Borra un partdo por ID
     * @param id del partido a borrar
     */
    public static void delete(int id){
        Session session = Conexion.getSession(); //Abrimos una sesión llamando a la clase conexion
        Transaction tx = session.beginTransaction(); //Creamos una transacción

        //Cadena con el delete:
        String hqlDel = "delete Partidos p where p.id= :pid";

        //Creamos la consulta a partir del String:
        Query qlDel = session.createQuery(hqlDel);

        //Especificamos los parámetros:
        qlDel.setInteger("pid", id);

        //Ejecutamos el Delete:
        int filasDel = qlDel.executeUpdate();
        
        try {

            tx.commit(); //Confirmamos la transacción

        }catch (ConstraintViolationException e) {

            System.out.println("ERROR");
            System.out.printf("MENSAJE: %s%n",e.getMessage());
            System.out.printf("COD ERROR: %d%n",e.getErrorCode());
            System.out.printf("ERROR SQL: %s%n" , e.getSQLException().getMessage());

        }
        session.close();
    
    }
}
