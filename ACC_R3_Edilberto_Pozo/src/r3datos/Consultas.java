/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package r3datos;

import java.util.Iterator;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author Usuario
 */
public class Consultas {
    
    /**
     * Consulta los campos y los equipos que tienen esos campos
     * @return un string
     */
    public static String conCampos(){
        
        Session session = Conexion.getSession();
        String hql = "from Equipos e, Campos c where e.idCampo = c.idCampo order by c.nombre";
        String rtn = " ";

        Query cons = session.createQuery(hql);

        Iterator q = cons.iterate();

        while (q.hasNext()) {

            Object[] ob = (Object[]) q.next();

            Equipos e = (Equipos) ob[0];

            Campos c = (Campos) ob[1];
                
            rtn = rtn+c.getNombre()+" --> "+e.getNombre()+" | Dirección: "+c.getDireccion()+" \n ";

        }
        session.close();
        return rtn;
    }
    
    /**
     * Consulta la media y el maximo de capacidad de los campos
     * @return un string
     */
      public static String conCapacidad(){
        
        Session session = Conexion.getSession();
        
        Query cons = session.createQuery("select avg(capacidad) from Campos");
        Double media = (Double) cons.uniqueResult();
        Query cons2 = session.createQuery("select max(capacidad) from Campos");

        Long max = (Long) cons2.uniqueResult();

        String rtn = " Media capacidad Campos: " + media+
                     " \n Máxima capacidad Campos: " + max;

        session.close();
    
        return rtn;
    }
   
        
    
}
