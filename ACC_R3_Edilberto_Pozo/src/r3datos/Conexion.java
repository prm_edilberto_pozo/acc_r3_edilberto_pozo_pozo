/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package r3datos;

import java.util.Date;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.exception.ConstraintViolationException;

/**
 *
 * @author Edilberto Pozo Pozo
 */
public class Conexion {
    /**
     * Crea la Session para conectar con la BBDD
     * @return Session 
     */
    public static Session getSession(){
            SessionFactory sesion = HibernateUtil.getSessionFactory();
            Session session = sesion.openSession(); //Abrimos una sesión llamando a la fábrica de sesiones
            return session;
    }
}
